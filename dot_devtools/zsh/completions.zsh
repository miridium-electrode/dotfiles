# exa
fpath=($ZDOTDIR/completions/exa $fpath)

# chezmoi
fpath=($ZDOTDIR/completions/chezmoi $fpath)

# rails
fpath=($ZDOTDIR/completions/rails $fpath)

# hugo
fpath=($ZDOTDIR/completions/hugo $fpath)

fpath=($HOME/.asdf/completions $fpath)

# Use modern completion system
zmodload zsh/complist
autoload -Uz compinit; compinit
_comp_options+=(globdots)	# include hidden files
zstyle ':completion:*' menu select