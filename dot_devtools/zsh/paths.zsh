# # android
# export ANDROID_SDK_ROOT="/extradroid/Android/Sdk"
# export ANDROID_SDK_HOME="/extradroid"
# export ANDROID_HOME="/extradroid/Android/Sdk"
# path+=("$ANDROID_HOME/tools" "$ANDROID_HOME/cmdline-tools/latest/bin")

# # clangd
# path+=("$DEVDIR/clangd/bin")

# # clojure
# path+=("$DEVDIR/clojure/bin" "$DEVDIR/lein")

# # cmake
# path+=("$DEVDIR/cmake/bin")
# export MANPATH="$MANPATH:$DEVDIR/cmake/man"

# # composer
# path+=("$DEVDIR/composer")

# # crystal
# path+=("$DEVDIR/crystal/bin")

# # dart-sass
# path+=("$DEVDIR/dart-sass")

# # Deno
# export DENO_INSTALL_ROOT=$DEVDIR/deno/script
# path+=("$DEVDIR/deno/bin" "$DENO_INSTALL_ROOT")

# # exa
# path+=("$DEVDIR/exa/bin")
# export MANPATH="$MANPATH:$DEVDIR/exa/man"

# # flutter
# path+=("$DEVDIR/flutter/bin")

# # go
# path+=("$DEVDIR/go/bin")

# # mintlang
# path+=("$DEVDIR/mint")

# # Ninja
# path+=("$DEVDIR/ninja")

# # nvm & node
# export NEXT_TELEMETRY_DISABLED=1

# # pure node
# NODE_VERSION=16
# path+=("$DEVDIR/node/v$NODE_VERSION/bin")

# # rust
# export CARGO_HOME="$DEVDIR/rust/cargo"
# export RUSTUP_HOME="$DEVDIR/rust/rustup"
# path+=("$CARGO_HOME/bin")

# # rvm ubuntu
# source "/etc/profile.d/rvm.sh

# asdf tool version manager
source $HOME/.asdf/asdf.sh

# local program
path+=("$HOME/.local/bin")

# # docker
# export DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker}

export PATH
