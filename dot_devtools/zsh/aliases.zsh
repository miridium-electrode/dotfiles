alias lr='exa'
alias la='exa -lah'
alias pathe="chezmoi edit $ZDOTDIR/paths.zsh"
alias aliase="chezmoi edit $ZDOTDIR/aliases.zsh"
alias zshrce="chezmoi edit $ZDOTDIR/.zshrc"
alias zshenve="chezmoi edit $HOME/.zshenv"
alias cigo='chezmoi'
alias reload="source $ZDOTDIR/.zshrc"
alias gitsavepwd='git config credential.helper cache'
alias gitcleanpwd='git credential-cache exit'
alias nvimc="$EDITOR $HOME/.config/nvim/init.lua"
alias nvimcdir="cd $HOME/.config/nvim"
alias bashspeed="for i in $(seq 1 10); do /usr/bin/time bash -i -c exit; done"
alias zshspeed="for i in $(seq 1 10); do /usr/bin/time zsh -i -c exit; done"
