function inoremap(to, from)
	vim.api.nvim_set_keymap('i', to, from, { noremap = true })
end

inoremap('hj', '<ESC>')
